from betamax import Betamax
from betamax_serializers import pretty_json


with Betamax.configure() as config:
    Betamax.register_serializer(pretty_json.PrettyJSONSerializer)
    config.default_cassette_options['serialize_with'] = 'prettyjson'
    config.preserve_exact_body_bytes = True
    # config.default_cassette_options['record_mode'] = 'none'
