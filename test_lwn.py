import logging
import shlex

import pytest
import sh
import xdg.BaseDirectory

import lwn

branches = ('tag0/foo', 'tag0/bar', 'tag0/baz',
            'tag1/foo', 'tag1/bar', 'tag1/baz')

logging.basicConfig(level='DEBUG')


@pytest.fixture(scope='session')
def fake_repo(tmpdir_factory):
    private = str(tmpdir_factory.mktemp('private'))
    sh.contrib.git('init', private)
    sh.contrib.git('-C', private, 'commit', '--allow-empty', '-m', 'test')
    for branch in branches:
        sh.contrib.git('-C', private, 'branch', branch)
    repo = tmpdir_factory.mktemp('repo')
    sh.contrib.git('clone', private, str(repo))
    return repo


def test_list(fake_repo, capfd):
    lwn.main(shlex.split('-c %s list' % fake_repo))
    out, err = capfd.readouterr()
    assert '0 branches found' == out.strip()

    lwn.main(shlex.split('-c %s list -r origin' % fake_repo))
    out, err = capfd.readouterr()
    assert '%d branches found' % (len(branches)+1) in out

    lwn.main(shlex.split('-c %s list -r origin tag0' % fake_repo))
    out, err = capfd.readouterr()
    assert '3 branches found' in out

    lwn.main(shlex.split('-c %s list -r origin -- -tag0' % fake_repo))
    out, err = capfd.readouterr()
    assert '4 branches found' in out
    assert 'tag0' not in out

    lwn.main(shlex.split('-c %s list -r origin -- -tag1' % fake_repo))
    out, err = capfd.readouterr()
    assert '4 branches found' in out
    assert 'tag1' not in out


@pytest.mark.usefixtures('betamax_session')
def test_get(fake_repo, betamax_session, monkeypatch, capfd):
    # safety precaution
    monkeypatch.setattr(lwn, 'load_cookiejar', lambda: '')
    cache = fake_repo.mkdir('cache')
    monkeypatch.setattr(xdg.BaseDirectory, 'xdg_cache_home', str(cache))

    path = fake_repo.join('offline-pgp.mdwn')
    args = lwn.parse_args(shlex.split('-c %s get --no-cookies -o %s 734767' % (str(fake_repo), str(path))))
    lwn.get(args, session=betamax_session)
    assert path.check(), "fetch works"
    assert '[[!meta' in path.read(), "looks like ikiwiki markup"
    assert '734767' in path.read(), "article ID present"
    path.remove()
    assert not path.check()

    args = lwn.parse_args(shlex.split('-c %s get --no-cookies 734767' % str(fake_repo)))
    lwn.get(args, session=betamax_session)
    assert path.check(), "cache works"

    path.remove()
    assert not path.check()
    args = lwn.parse_args(shlex.split('-c %s get --no-cookies -o - 734767' % str(fake_repo)))
    del betamax_session.cookies['sub_nag']
    lwn.get(args, session=betamax_session)
    assert not path.check()
    out, err = capfd.readouterr()
    assert '734767' in out
    assert '[[!meta' in out
