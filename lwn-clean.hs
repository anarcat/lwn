#!/usr/bin/env runhaskell

-- cleanup cruft in pandoc's output during HTML to Markdown conversion
-- of LWN articles

-- this can be called with:
-- pandoc -f html -t markdown --filter ~/bin/lwn-clean.hs

-- this removes the copyright statement which is only relevant for my
-- personal use, for the articles which I am the actual author.

import Data.Char ( ord, isDigit )
import Data.List ( isPrefixOf )
import Text.Pandoc.JSON
import Text.Pandoc.Generic
import Data.Time (ZonedTime, getZonedTime)
import Data.Time.Format (defaultTimeLocale, parseTimeOrError, formatTime)

-- inline rules
cleanInline :: Inline -> [Inline]
-- remove empty links and make relative links absolute
cleanInline (Link attrs txt (url, title)) | url == "" = txt
                                          | isPrefixOf "/" url = [Link attrs txt ("https://lwn.net" ++ url, title)]
-- remove image attributes like width/height that don't work in ikiwiki
cleanInline (Image _ txt (url, title)) = [Image ("", [], []) txt (url, title)]
-- remove spurious line breaks
cleanInline (LineBreak) = []
-- make sure quotes are properly styled
cleanInline (Span _ x) = [Emph x]

-- pass through anything else
cleanInline x = [x]

-- block rules
cleanBlock :: Block -> [Block]
-- fix quotes
cleanBlock (Div (id, [cls], _) body) | cls == "BigQuote" = [BlockQuote body]
                                     -- remove useless containers
                                     | cls == "navmenu-container" = []
                                     | cls == "topnav-container" = []
                                     | cls == "not-print" = []
                                     | cls == "MakeALink" = []
                                     | cls == "CommentNotify" = []
                                     | cls == "ErrorWords" = []
                                     | cls == "CommentBox" = []
-- remove menu
cleanBlock (Div (id, [], _) body) | id == "menu" = []
-- remove remaining div markup
cleanBlock (Div _ body) = body
-- remove empty tables
cleanBlock (Table _ _ _ _ [[cell]]) | cell == [] = []
-- remove needless blockquote wrapper around some tables
--
-- haskell newbie tips:
--
-- @ is the "at-pattern", allows us to define both a name for the
-- construct and inspect the contents as once
--
-- {} is the "empty record pattern": it basically means "match the
-- arguments but ignore the args"
cleanBlock (BlockQuote t@[Table {}]) = t
-- headers are too deep
cleanBlock (Header depth attrs text) | depth == 1 = [Plain ([RawInline (Format "html") "[[!meta title=\""]
                                                            ++ text
                                                            ++ [RawInline (Format "html") "\"]]"])]
                                     | depth == 4 = [Header 2 attrs text]
cleanBlock (Plain (Strong (Str "Notifications":Space:Str "for":Space:Str "all":Space:Str "responses":_):_)) = []
-- pass through anything else
cleanBlock x = [x]

-- remove LWN copyright notice, I run this on material I own
cleanCopy :: [Inline] -> [Inline]
cleanCopy (Str "Copyright":Space:Str ds:xs) = []
cleanCopy x = x

-- format the given time as a ISO-8601 string
iso8601Format :: ZonedTime -> String
iso8601Format time = formatTime defaultTimeLocale "%Y-%m-%dT%H:%M:%S%z" time

-- format the given date as a ikiwiki metadata field
ikiwikiMetaField :: String -> String -> String
ikiwikiMetaField key value = "[[!meta " ++ key ++ "=\"" ++ value ++ "\"]]"

-- format an arbitrary string as raw ikiwiki code
--
-- we abuse the "raw html" pandoc formatter here otherwise the square
-- brackets get escaped as \[\[!...\]\]
ikiwikiRawInline :: String -> [Block]
ikiwikiRawInline what = [Plain [RawInline (Format "html") what]]

-- the "GAByline" div has a date, use it to generate the ikiwiki dates
--
-- this is distinct from cleanBlock because we do not want to have to
-- deal with time there: it is only here we need it, and we need to
-- pass it in here because we do not want to mess with IO (time is I/O
-- in haskell) all across the function hierarchy
cleanDates :: ZonedTime -> Block -> [Block]
-- this mouthful is just the way the data comes in from
-- LWN/Pandoc. there could be a cleaner way to represent this,
-- possibly with a record, but this is complicated and obscure enough.
cleanDates time (Div (_, [cls], _)
                 [Para [Str month, Space, Str day, Space, Str year], Para _])
  | cls == "GAByline" = ikiwikiRawInline (ikiwikiMetaField "date"
                                           (iso8601Format (parseTimeOrError True defaultTimeLocale "%Y-%B-%e,"
                                                           (year ++ "-" ++ month ++ "-" ++ day) :: ZonedTime)))
                        ++ ikiwikiRawInline (ikiwikiMetaField "updated"
                                             (iso8601Format time))
                        ++ [Para []]
                        ++ ikiwikiRawInline "[[!toc levels=2]]"
                        ++ [Para []]

-- other elements just pass through
cleanDates time x = [x]

-- wrapper for the block and inline functions
cleanAll :: ZonedTime -> Pandoc -> Pandoc
cleanAll time = bottomUp (concatMap cleanInline) . bottomUp (concatMap cleanBlock) . bottomUp (concatMap (cleanDates time)) . bottomUp cleanCopy

-- run it all at once
main :: IO ()
main = do
  time <- getZonedTime
  toJSONFilter (cleanAll time)
