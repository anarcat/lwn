#!/usr/bin/python3

'''LWN blogging interface'''

import argparse
from collections import OrderedDict
import configparser
import logging
import json
import os.path
import re
import sqlite3
import sys
import tempfile  # noqa

import requests
import sh
import xdg.BaseDirectory as xdg_base_dir


def main(args):
    '''Various utilities to work with my LWN articles and workflow.'''
    args = parse_args(args)
    logging.basicConfig(level=args.loglevel)
    # special configuration for sh module to silence more too intense
    # debug logging. but still a bit too noisy:
    # https://github.com/amoffat/sh/issues/409
    level = logging.getLevelName(args.loglevel)+10  # DEBUG -> INFO
    logging.getLogger('sh').setLevel('WARNING')
    logging.getLogger('sh.command').setLevel(level)

    if 'func' in args:
        try:
            args.func(args)
        except sh.ErrorReturnCode as e:
            logging.error('command failed with exit code %d: %s', e.exit_code, e.full_cmd)
            logging.error('stderr: %s', e.stderr)


class NegateAction(argparse.Action):
    '''add a toggle flag to argparse

    this is similar to 'store_true' or 'store_false', but allows
    arguments prefixed with --no to disable the default. the default
    is set depending on the first argument - if it starts with the
    negative form (defined by default as '--no'), the default is False,
    otherwise True.

    originally written for the stressant project.
    '''

    negative = '--no'

    def __init__(self, option_strings, *args, **kwargs):
        '''set default depending on the first argument'''
        default = not option_strings[0].startswith(self.negative)
        super(NegateAction, self).__init__(option_strings, *args,
                                           default=default, nargs=0, **kwargs)

    def __call__(self, parser, ns, values, option):
        '''set the truth value depending on whether
        it starts with the negative form'''
        setattr(ns, self.dest, not option.startswith(self.negative))


def parse_args(args):
    parser = argparse.ArgumentParser(description=__doc__, epilog=main.__doc__)
    logging_args(parser)
    parser.add_argument('-c', '--chdir', default='~/wikis/anarc.at',
                        help='operate from given directory (default: %(default)s)')
    subparsers = parser.add_subparsers(title='Commands')

    get_parser = subparsers.add_parser('get', help=get.__doc__,
                                       description=get.__doc__,
                                       epilog='Example: lwn get -o ~/wikis/anarc.at/blog/primes.mdwn -b primes 738896')
    get_parser.set_defaults(func=get)
    get_parser.add_argument('url', help='LWN article URL or ID', nargs='?')
    get_parser.add_argument('-o', '--output', default=None, metavar='FILE',
                            help='output file, use - for stdout (default: look in cache)')
    get_parser.add_argument('-d', '--diff', action='store_true',
                            help='show a colored diff of the changes')
    get_parser.add_argument('-m', '--commit', nargs='?', default=None, const=True, metavar='MESSAGE',
                            help='commit the result, with message if provided')
    get_parser.add_argument('-b', '--branch', help='switch to branch first (default: no change)')
    get_parser.add_argument('--cookies', '--no-cookies', action=NegateAction,
                            help='send cookies found in Firefox profile (default: %(default)s)')
    get_parser.add_argument('--cookiejar-path', metavar='PATH',
                            help='provide absolute path to Firefox cookiejar (default: %(default)s)')
    get_parser.add_argument('--firefox-home', metavar='DIR',
                            default=os.path.expanduser('~/.mozilla/firefox'),
                            help='path to the Firefox profile base directory (default: %(default)s)')

    list_parser = subparsers.add_parser('list', aliases=['ls'], help=list_f.__doc__,
                                        description=list_f.__doc__,
                                        epilog='Show branches matching given pattern on such remote')
    list_parser.set_defaults(func=list_f)
    list_parser.add_argument('-r', '--remote', default='private',
                             help='restritct to given remote (default: %(default)s)')
    list_parser.add_argument('pattern', default=['-fin', '-attic'], nargs='*',
                             help='branch prefix to match, use - to negate (default: %(default)s)')

    publish_parser = subparsers.add_parser('publish', help=publish.__doc__,
                                           description=publish.__doc__,
                                           epilog='Publish a branch to the blog (not implemented)')
    publish_parser.set_defaults(func=publish)

    help_parser = subparsers.add_parser('help', help='show help')
    help_parser.set_defaults(func=lambda args: parser.print_help())

    args = parser.parse_args(args)
    args.chdir = os.path.expanduser(args.chdir)
    if 'pattern' in args:
        args.exclude = [x[1:] for x in args.pattern if x.startswith('-')]
        args.pattern = [x for x in args.pattern if not x.startswith('-')]
    return args


def list_f(args):
    '''list known articles'''
    i = -1
    for i, match in enumerate(list_branches(args.chdir, remote=args.remote,
                                            patterns=args.pattern, exclude=args.exclude)):
        print(match.group(0))
    print('%d branches found' % (i+1))


def list_branches(chdir, remote=None, patterns=None, exclude=None):
    '''ask git which branches are available, matching a given remote and "pattern"'''
    regex = re.compile(r'^\s+remotes/(?P<remote>[^/]*)/(?P<branch>(?:(?P<pattern>[^/]+)/)?(?P<name>[\w-]+))\s+(?P<commit>\w+)\s+(?P<log>.*)$')  # noqa
    for line in sh.contrib.git('-C', chdir, 'branch', '-a', '--color=never', '-v', _iter=True):
        match = regex.match(line)
        if not match:
            logging.debug('discarding line "%s" not matching regex %s', line.strip(), regex)
            continue
        matches = match.groupdict()
        if patterns and matches['pattern'] not in patterns:
            logging.debug('discarding match %s not matching patterns %s', match, patterns)
            continue
        if exclude and matches['pattern'] in exclude:
            logging.debug('discarding match %s matching exclude %s', match, patterns)
            continue
        if remote and matches['remote'] != remote:
            logging.debug('discarding match %s not in remote %s', match, remote)
            continue
        yield match


def get(args, session=None):
    '''download and convert LWN article'''
    if args.branch:
        for match in list_branches(args.chdir):
            branch = match.groupdict()['branch']
            if args.branch in branch:
                print(sh.contrib.git(['-C', args.chdir, 'checkout', '--quiet', branch]))

    if session is None:  # pragma: nocover
        session = requests.Session()
    if args.cookies:  # pragma: nocover
        cookiejar_path = find_cookiejar(args.firefox_home)
        cookie = load_cookiejar(cookiejar_path)
        session.cookies['LWNSession1'] = cookie
    if '://' not in args.url:
        args.url = 'https://lwn.net/Articles/%s/' % args.url
    content = load_article(args.url, session)
    result = parse(content, args.url)
    if 'static.lwn.net' in result:
        # XXX: not implemented
        logging.warning('image detected, not supported. consider fixing link to local image instead of hotlinking to LWN.net')
    pattern = re.compile(r'(?<!^\[first appeared]: https://)lwn.net/Articles', re.MULTILINE)
    for match in pattern.finditer(result):
        start = result[0:match.start()]
        end = result[0:match.end()]
        startline = start.count("\n")
        endline = end.count("\n")
        lines = result.split("\n")
        if 'static.lwn.net' not in lines[startline]:
            linewarning = 'on line %d' % startline
            if startline != endline:
                linewarning = 'on lines %d-%d' % (startline, endline)
            # XXX: not implemented
            logging.warning('link to lwn.net may need correction %s: %s', linewarning, "\n".join(lines[startline:endline+1]))
    if args.output == '-':
        sys.stdout.write(result)
        return
    path = guess_path(args.url, args.output)
    if not path:
        logging.error('no path provided, none found in cache, exiting')
        return
    logging.info('writing converted article to %s from URL %s', path, args.url)
    with open(path, 'w') as output:
        output.write(result)
    if args.diff:
        sh.git(['-C', args.chdir, 'diff', '--color-words'], _fg=True)
    if args.commit:
        command = ['-C', args.chdir, 'commit', path]
        if args.commit is not True:
            command += ['-m', args.commit]
        print(sh.contrib.git(command))


def publish(args, session=None):
    '''publish given branch'''
    logging.warning('publish not implemented')
    print('''here is the procedure I follow while this gets implemented. this
example was done when the Prometheus series was published and is a
good corner case but tries to cover other more normal scenarios as well.

 1. find which articles are on the branch:
 
         git diff --name-only $(git merge-base --all HEAD master)
 
     but note that it might be more complicated in the general
     case: https://stackoverflow.com/questions/1527234/finding-a-branch-point-with-git
 
 2. get the articles
 
         lwn  get -d -m'prom article online' 744410
         lwn  get -d -m'prom article online' 744721
 
 3. edit the articles to fix various formattings issues:

    * conference series have the wrong date
    * fix LWN links (as advised by get)
    * add tags
    * look for backslashes: \$ and \" used to be a problem but were
      fixed in parse_fixup

 4. check if the articles are online (i was late, so both were online
    at once. if i hadn't been, i would have had to split the branches
    here, which is harder as it involves a filter-branch thing).
    example filter-branch command, for a split:
 
         git filter-branch --prune-empty -f --index-filter 'git rm --cached --ignore-unmatch blog/other.mdwn blog/secret-notes.mdwn' branch-point..HEAD
 
    interesting notes: blog/other.mdwn will show up in the above "diff
    $(merge-base)", but not blog/secret-notes.mdwn, because the latter
    may have been removed already.
 
 5. look at history to see if there's anything to hide (git log
    --stat? non-trivial, so needs to be interactive, to spot problems
     mentioned above)
 
 6. rename files with dates: filename should be the current date (we
    used to use the LWN publication date, but this would create
    problems in Ikiwiki as the file ctime is used for the "creation
    date", regardless of the "data" meta sometimes?. should be part of
    "get"

         git mv blog/changes-prometheus-2.0.mdwn  blog/2018-01-25-changes-prometheus-2.0.mdwn
 
 7. include article in series if relevant
 
 6. commit & push the resulting branch to private
 
 7. merge the branch in master, squashing history if stuff needs hiding:

         git co master; git merge --squash -
 
 8. if history was kept (ie. no squash), delete the branch:

         git branch -d -; git push private :backlog/prometheus

    if history was removed, rename branch to `fin`:
 
         git branch -m backlog/article fin/article
         git push private :backlog/article fin/article -u
         git branch -d fin/article
 
 11. git push master
 
 12. remove the article from cache?
''')


def guess_path(url, path):
    cache = JsonCache('lwn')
    if path:
        logging.info('saving path %s cache for URL %s', path, url)
        cache[url] = os.path.abspath(path)
        cache.commit()
    else:
        path = cache.get(url)
    return path


class JsonCache(OrderedDict):
    """Simple file-backed JSON cache.

    >>> tmpfile = tempfile.NamedTemporaryFile()
    >>> j = JsonCache('test', tmpfile.name)
    >>> j['a'] = 1
    >>> j.commit()
    >>> with open(tmpfile.name) as fp:
    ...     assert fp.read() == '{"a": 1}'
    >>> j["b"] = 'haha'
    >>> j.commit()
    >>> with open(tmpfile.name) as fp:
    ...     assert fp.read() == '{"a": 1, "b": "haha"}'
    >>> tmpfile.close()
    """
    def __init__(self, name, path=None):
        if path is None:
            path = os.path.join(xdg_base_dir.xdg_cache_home, '%s.js' % name)
        self._path = path
        if os.path.exists(self._path) and os.path.getsize(self._path) > 0:
            logging.debug('loading cache from %s', self._path)
            with open(self._path) as fp:
                self.update(json.load(fp))
                self._path = path

    def commit(self):
        logging.debug('writing cache to %s', self._path)
        with open(self._path, 'w') as fp:
            json.dump(self, fp)


def logging_args(parser):
    """
    >>> parser = argparse.ArgumentParser()
    >>> logging_args(parser)
    >>> parser.parse_args(['--verbose'])
    Namespace(loglevel='INFO')
    >>> parser.parse_args(['--verbose', '--debug'])
    Namespace(loglevel='DEBUG')
    """
    default_level = 'WARNING'
    parser.add_argument('-v', '--verbose',
                        dest='loglevel', action='store_const',
                        const='INFO', default=default_level,
                        help='enable verbose messages')
    parser.add_argument('-d', '--debug',
                        dest='loglevel', action='store_const',
                        const='DEBUG', default=default_level,
                        help='enable debugging messages')
    parser.add_argument('--loglevel', choices=logging._nameToLevel.keys(),
                        default=default_level, type=str.upper,
                        help='expliticly set logging level (default: %(default)s)')


def load_article(url, session):
    """download article from LWN"""
    if url:
        response = session.get(url)
        response.raise_for_status()
        logging.debug('got %d bytes from requests', len(response.content))
        return response.content
    else:
        return sys.stdin.read()


def parse(content, url):
    """parse the HTML content back into Markdown"""
    filter_args = ['-f', 'html', '-t', 'markdown-simple_tables-shortcut_reference_links',
                   '--reference-links', '--reference-location', 'section',
                   '--filter', 'lwn-clean.hs']
    result = sh.pandoc(filter_args, _in=content)
    footer = '''
> *This article [first appeared][] in the [Linux Weekly News][].*

[first appeared]: {url}
[Linux Weekly News]: http://lwn.net/

[[!tag debian-planet lwn]]
'''.format(url=url)
    return parse_fixup(result.stdout.decode('utf-8')) + footer


def parse_fixup(content):
    """fixup the rendered Markdown from Pandoc

    This should be in lwn-clean.hs, but we can't figure out how to do
    it there.
    """
    return re.sub(r'\\([\'"$])', r'\1', content)


def find_cookiejar(firefox_home):  # pragma: nocover
    """load LWN session cookie from Firefox cookie jar"""
    config = configparser.ConfigParser()
    config.read(os.path.join(firefox_home, 'profiles.ini'))
    for profile in config:
        if (config[profile].get('Default') == '1'):
            path = config[profile].get('Path')
            break
    else:
        logging.warning('no default Firefox profile found')
        return None
    return os.path.join(firefox_home, path, 'cookies.sqlite')


def load_cookiejar(path):
    logging.debug('loading database %s', path)
    conn = sqlite3.connect(path)
    try:
        conn.set_trace_callback(logging.debug)
    except AttributeError:  # pragma: nocover
        logging.debug('no logging support in sqlite')
    c = conn.cursor()
    c.execute('SELECT value FROM moz_cookies'
              ' WHERE baseDomain LIKE "lwn.net"'
              ' AND name LIKE "LWNSession1"'
              ' ORDER BY lastAccessed'
              ' DESC LIMIT 1;')
    cookie = c.fetchone()
    if cookie is None:
        logging.warning('no valid cookie found in database %s', path)
        return None
    logging.debug('found cookie %s', cookie[0])
    return cookie[0]


if __name__ == '__main__':  # pragma: nocover
    main(sys.argv[1:])
